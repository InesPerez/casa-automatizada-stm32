/*
 * oled.h
 *
 *  Created on: Jan 20, 2021
 */

//OLED
/*Gestiona las salidas por pantalla*/

#ifndef INC_OLED_H_
#define INC_OLED_H_
#include "fonts.h"
#include "ssd1306.h"
#include "bitmap.h"

typedef enum {REAL, CLIMATIZADOR} types;

//GESTIÓN DISPLAY DE TEMPERATURAS
/*Transforma el entero propocionado en una cadena de caracteres que
 * pueda utilizar la librería ssd1306.h*/

void Print_Temperature(types t,int temp){
	char cad[20]="   C";
	int minus=0;
	if (temp<0){//Temperatura bajo cero
		temp=abs(temp);
		minus=1;
	}
	int d=temp/10;
	int u=temp%10;

	if(!minus && temp<100){
		cad[0]=d+'0';
		cad[1]=u+'0';
	}
	else if(minus && temp<10){
		cad[0]='-';
		cad[1]=u+'0';
	}
	else{
		cad[0]=' ';
		cad[1]='0';
	}

	if(t==CLIMATIZADOR){//Salida por pantalla
		SSD1306_GotoXY (20,53);
		SSD1306_Puts (cad, &Font_7x10, 1);
		SSD1306_DrawCircle(37, 53, 2, 1);//º
	}
	if(t==REAL){
		SSD1306_GotoXY (55,37);//Salida por pantalla
		SSD1306_Puts (cad, &Font_16x26, 1);
		SSD1306_DrawCircle(95, 40, 4, 1);
	}

	SSD1306_DrawBitmap(0,0,termometro, 128, 64, 1);//Icono temperatura
	SSD1306_UpdateScreen();//Actualización bus i2c
}

//GESTIÓN HUMEDAD
/*Transforma el entero propocionado en una cadena de caracteres que
 * pueda utilizar la librería ssd1306.h*/

void Print_Humidity(int hum){
	char cad[20]="   %";
	int d=hum%100/10;
	int u=hum%10;

	if(hum>=0 && hum<100){
		cad[0]=d+'0';
		cad[1]=u+'0';
	}
	else {//desbordamiento
		cad[0]='0';
		cad[1]='0';
	}

	//Salida por pantalla
	SSD1306_GotoXY (97,20);
	SSD1306_Puts (cad, &Font_7x10, 1);

	SSD1306_DrawBitmap(3,0,gota, 128, 64, 1);//Icono humedad
	SSD1306_UpdateScreen();//Actualización bus i2c
}

//GESTIÓN PRESIÓN
/*Transforma el entero propocionado en una cadena de caracteres que
 * pueda utilizar la librería ssd1306.h*/
void Print_Pressure(int pre){
	char cad[20]="    bar";
	int c=pre/100;
	int d=pre%100/10;
	int u=pre%10;

	if(pre>=0 && pre<1000){
		cad[0]=c+'0';
		cad[1]=d+'0';
		cad[2]=u+'0';
	}
	else{
		cad[0]='0';
		cad[1]='0';
		cad[2]='0';
	}

	//Salida por pantalla
	SSD1306_GotoXY (33,20);
	SSD1306_Puts (cad, &Font_7x10, 1);
	SSD1306_UpdateScreen();//Actualización bus i2c
}

//ICONO LUZ
void PrintLight(int active){
	SSD1306_DrawBitmap(0,0,bombilla, 128, 64, active);
	SSD1306_UpdateScreen();
}

//ICONO FRÍO
/*Imprime por pantalla el icono del aire acondicionado y borra el de la calefacción*/

void PrintCold(int active){
	SSD1306_DrawBitmap(0,0,calefaccion, 128, 64, 0);
	SSD1306_DrawBitmap(0,0,copo_nieve, 128, 64, active);
	SSD1306_UpdateScreen();//Actualización bus i2c
}

//ICONO CALOR
/*Imprime por pantalla el icono de la calefacción y borra el del aire acondicionado*/

void PrintWarm(int active){
	SSD1306_DrawBitmap(0,0,copo_nieve, 128, 64, 0);
	SSD1306_DrawBitmap(0,0,calefaccion, 128, 64, active);
	SSD1306_UpdateScreen();//Actualización bus i2c
}

//MODO
//Imprime por pantalla el modo del climatizador

void clim_opt(int active){

	if(active){
		SSD1306_GotoXY (0,20);
		SSD1306_Puts ("AUTO", &Font_7x10, 1);
	}
	else{
		SSD1306_GotoXY (0,20);
		SSD1306_Puts ("OFF ", &Font_7x10, 1);
	}

	SSD1306_UpdateScreen();//Actualización bus i2c
}


#endif /* INC_OLED_H_ */
