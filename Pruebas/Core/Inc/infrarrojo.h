/*
 * infrarrojo.h
 *
 *  Created on: Jan 29, 2021
 */

#ifndef INC_INFRARROJO_H_
#define INC_INFRARROJO_H_

#include "dwt_stm32_delay.h"

int* var1;
int* var2;
int* var3;
int* var4;
int* var5;

void variables(int* v1, int* v2, int* v3, int* v4,int* v5){
	var1=v1;
	var2=v2;
	var3=v3;
	var4=v4;
	var5=v5;
}

uint32_t lectura_ir(void)
{
	  uint32_t lectura=0;
	  //Primero hay que esperar a que el pin se ponga en HIGH durante 9ms, y después en LOW durante 4.5ms, lo que indica el inicio del envío de datos.
	  while(!(HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_4)));
	  uint8_t contador=0;
	  while((HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_4))){
		  contador++;
		  DWT_Delay_us(1000);
		  if (contador>10)
			  return 0;
	  }
	  //Después el sensor envía una señal de 32 bits
	  for(int i=0; i<32; i++)
	  {

		 while(!(HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_4))); //Esperas a que se ponga en HIGH
		 contador=0;
		 while((HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_4))) //Esperas a que se ponga en LOW de nuevo
		 {
			 contador++;
			 DWT_Delay_us(100);
			 if (contador>50)
				 return 0;
		 }

		 if(contador <= 12){
			 lectura &= ~(1UL << (31-i)); //Si está en low menos de 1.2ms, es un 0, se escribe un 0 en el bit correspondiente
		 }
		 else
			 lectura|= (1UL << (31-i)); //Si está en low más de 1.2ms, es un 1 y se escribe un 1 en el bit correspondiente
	  }
	  return lectura;
}

void decoder_ir(uint32_t datos)
{
	switch(datos)
	{
	//Pulsa 1
	case 0xFF30CF:
		*var3=!*var3;
		HAL_GPIO_WritePin(GPIOD,GPIO_PIN_15,*var3);
		break;

	//Pulsa 2
	case 0xFF18E7:
		*var4=!*var4;
		HAL_GPIO_WritePin(GPIOD,GPIO_PIN_13,*var4);
		break;

	//Pulsa 3
	case 0xFF7A85:
	break;

	//Pulsa 4
	case 0xFF10EF:
	break;

	//Pulsa 5
	case 0xFF38C7:
	break;

	//Pulsa 6
	case 0xFF5AA5:
	break;

	//Pulsa 7
	case 0xFF42BD:
	break;

	//Pulsa 8
	case 0xFF4AB5:
	break;

	//Pulsa 9
	case 0xFF52AD:
	break;

	//Pulsa 0
	case 0xFF6897:
		*var2=!*var2;
		HAL_GPIO_WritePin(GPIOD,GPIO_PIN_14,!*var2);
		HAL_GPIO_WritePin(GPIOD,GPIO_PIN_12,*var2);
		break;

	//Pulsa flecha abajo
	case (0xFFE01F):
		break;

	//Pulsa flecha arriba
	case (0xFF906F):
		break;

	//Pulsa VOL+
	case (0xFF629D):

		if(*var5<30 && *var5>=15){
			*var5=*var5+1;
		}
		break;
	//Pulsa VOL-
	case (0xFFA857):
		if(*var5<=30 && *var5>15){
			*var5=*var5-1;
		}
		break;
	//Pulsa POWER
	//0xFFE17B7F
	case (0xFFA25D):
			*var1=!*var1;
		break;
	//Pulsa Fast Back
	case (0xFF22DD):
		break;
	//Pulsa Fast Forward
	case (0xFFC23D):
		break;
	//Pulsa Play/Pause
	case (0xFF02FD):
		break;
	//Pulsa Func/Stop
	case (0xFFE21D):
		break;
	//Pulsa EQ
	case (0xFF9867):
		break;
	//Pulsa ST/REPT
	case (0xFFB04F):
		break;
	//Se repite
	case 0xFFFFFFFF:
	break;

	default :
		break;
	}
}
#endif /* INC_INFRARROJO_H_ */
