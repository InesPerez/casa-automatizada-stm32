/*
 * temperatura.h
 *
 *  Created on: Jan 20, 2021
 */

#ifndef INC_TEMPERATURA_H_
#define INC_TEMPERATURA_H_

#include "oled.h"

/////////////////////////////////////
//SENSOR INTERNO TEMPERATURA/////////
//Valores comprobados con termometro
/*no son valores lineales y varían mucho cada vez que se reinicia la placa.
 * Por ello se ha utilizado otro tipo de sensor*/
#define N 5
float V_i[N]={1019,1026,1034,1040,1045};
float temp_i[N]={14.2,16.2,17.6,19.8,21.6};

//SENSOR EXTERNO TEMPERATURA/////////
/*El sensor ntc es dificil de implementar en el montaje.*/
float vcc=3;
float bit=4096;
float R25=80;
float T25=298;
float B=3950;
float R=1000;

//Estructura para controlar valores umbral en adc
typedef struct{
	float  sr_max;
	float sr_min;
	float value_max;
	float value_min;
}adc_sensor;

adc_sensor pot={3900,500,30,15};//potenciometro

//OBTENCIÓN VALOR DE TEMPERATURA

int get_temp (types t,float variable){
	float value=0;
	if (t==REAL){//Temperatura REAL (sensor de la placa)
		for(int i=1;i<N;i++)
			if (variable < V_i[i] || i==N-1){
				value=(variable-V_i[i-1])/((V_i[i]-V_i[i-1])/(temp_i[i]-temp_i[i-1]))+temp_i[i-1];
				break;
			}
		//value=1/(log((vcc-variable/bit*vcc)/(variable/bit*vcc/R)/R25)/B+(1/T25))-273;//formula ntc
	}
	else if (t==CLIMATIZADOR){//Temperatura deseada (potenciómetro)
		value=(pot.value_max-pot.value_min)/(pot.sr_max-pot.sr_min)*(variable-pot.sr_min)+pot.value_min;
		if (value<pot.value_min) value=pot.value_min;
		else if (value>pot.value_max) value=pot.value_max;
	}

	return (int)value;
}



#endif /* INC_TEMPERATURA_H_ */
