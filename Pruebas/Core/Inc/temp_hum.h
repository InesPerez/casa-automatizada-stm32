/*
 * temp_hum.h
 *
 *  Created on: Feb 1, 2021
 */

#ifndef INC_TEMP_HUM_H_
#define INC_TEMP_HUM_H_

#include <stdlib.h>
#include <string.h>
#include "bme280.h"

I2C_HandleTypeDef* hi2c;

struct bme280_dev dev;
struct bme280_data comp_data;
int8_t rslt;
uint32_t fin=0;

int8_t bme280_i2c_read(uint8_t id, uint8_t reg_addr, uint8_t *data, uint16_t len)
{
  if(HAL_I2C_Master_Transmit(hi2c, (id << 1), &reg_addr, 1, 10) != HAL_OK) return -1;
  if(HAL_I2C_Master_Receive(hi2c, (id << 1) | 0x01, data, len, 10) != HAL_OK) return -1;

  return 0;
}

void bme280_delay_ms(uint32_t period)
{
	uint32_t start = HAL_GetTick();
	uint8_t bool=0;
  while(!bool)
  {
	  if(HAL_GetTick()-start>=period)
		  bool=1;
  }
}

int8_t bme280_i2c_write(uint8_t id, uint8_t reg_addr, uint8_t *data, uint16_t len)
{
  int8_t *buf;
  buf = malloc(len +1);
  buf[0] = reg_addr;
  memcpy(buf +1, data, len);

  if(HAL_I2C_Master_Transmit(hi2c, (id << 1), (uint8_t*)buf, len + 1, HAL_MAX_DELAY) != HAL_OK) return -1;

  free(buf);
  return 0;
}

void bme280_Init(I2C_HandleTypeDef* handler){
	hi2c=handler;
	dev.dev_id = BME280_I2C_ADDR_PRIM;
	dev.intf = BME280_I2C_INTF;
	dev.read = bme280_i2c_read;
	dev.write = bme280_i2c_write;
	dev.delay_ms = bme280_delay_ms;

	rslt = bme280_init(&dev);

	dev.settings.osr_h = BME280_OVERSAMPLING_1X; //Oversampling x1 en humedad
	dev.settings.osr_p = BME280_OVERSAMPLING_16X; //Oversampling x16 en presión
	dev.settings.osr_t = BME280_OVERSAMPLING_2X; //Oversampling x2 en temperatura
	dev.settings.filter = BME280_FILTER_COEFF_16; //Coeficiente del filtro
	rslt = bme280_set_sensor_settings(BME280_OSR_PRESS_SEL | BME280_OSR_TEMP_SEL | BME280_OSR_HUM_SEL | BME280_FILTER_SEL, &dev); //Ajustes del sensor
}

void bme280_value(float* temperatura, float* humedad, float* presion){
	if(HAL_GetTick()-fin>1000){
		//Primera medida
		rslt = bme280_set_sensor_mode(BME280_FORCED_MODE, &dev);
		dev.delay_ms(40);
		// Obtener medidas
		rslt = bme280_get_sensor_data(BME280_ALL, &comp_data, &dev);
		if(rslt == BME280_OK){
			*temperatura = comp_data.temperature / 100.0;      // Ajuste datos a grados celsius
			*humedad = comp_data.humidity / 1024.0;           // Rsultado humedad en porcentaje
			*presion = comp_data.pressure / 10000.0;          // hPa = bar
		}
		fin=HAL_GetTick();
	}
}

#endif /* INC_TEMP_HUM_H_ */
