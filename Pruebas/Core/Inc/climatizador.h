/*
 * climatizador.h
 *
 *  Created on: Jan 20, 2021
 */

#ifndef INC_CLIMATIZADOR_H_
#define INC_CLIMATIZADOR_H_

#include "oled.h"

typedef enum {OFF, ON} options;
int led_max=100;

//CLIMATIZADOR//
/*Compara los valores de la temperatura deseada y real,
 * y decide si encender el aire acondicionado o la calefacción*/

void climatizador(options opt, uint8_t real, uint8_t clim,uint8_t clim_i){
	uint8_t c=clim;
	if(clim != clim_i)
		c=clim_i;
	clim_opt(opt);
	if(real<c) PrintWarm(opt);
	else if(real>c) PrintCold(opt);
}

// CONTROL PWM
/*Si la luz está encendida (en=ON) proporciona corriente al LED.
 *Si el modo PWM está encendido, ésta está definida por el valor del regulador [0,Periodo]*/

void pwm(TIM_HandleTypeDef* htim,int max,int regulador,options opt_pwm, options en){
	htim->Init.Period=max;
	if (en==ON){
		if (opt_pwm==ON){
			if (regulador<max)
				__HAL_TIM_SET_COMPARE(htim, TIM_CHANNEL_3,htim->Init.Period-regulador);
			else
				__HAL_TIM_SET_COMPARE(htim, TIM_CHANNEL_3,0);
		}
		else if (opt_pwm==OFF){
			__HAL_TIM_SET_COMPARE(htim, TIM_CHANNEL_3,htim->Init.Period);
		}
	}
	else if (en==OFF)
		__HAL_TIM_SET_COMPARE(htim, TIM_CHANNEL_3,0);
}

// CONTROL LUZ
/*Gestiona el control PWM y la activación del icono*/

void luz(TIM_HandleTypeDef* htim,options active,options opt_pwm,int reg){
	PrintLight(opt_pwm);
	pwm(htim,led_max,reg,opt_pwm, active);
}

#endif /* INC_CLIMATIZADOR_H_ */
